import unittest
import app


class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_add(self):
        test = {'a': 1, 'b': 2}
        result = self.app.get('/add', query_string=test)
        result = result.get_data().decode()
        self.assertEqual(str(3), result)

    def test_sub(self):
        test = {'a': 3, 'b': 5}
        result = self.app.get('/sub', query_string=test)
        result = result.get_data().decode()
        self.assertEqual(str(-2), result)


if __name__ == "__main__":
    unittest.main()
